package org.acme.resteasy.recourses;

import org.acme.resteasy.model.Class;
import org.acme.resteasy.model.Student;
import org.acme.resteasy.service.ClassService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
public class ClassResourceTest {
    ClassResource classResource;
    @Mock
    ClassService classService;
    @BeforeEach
    public void setup(){
        classResource=new ClassResource();
        classService=mock(ClassService.class);

    }
//get class test
    @Test
    @Tag("get")
    public void getClassWillThrowExceptionIfNoClassIsAvaliable(){
        //ClassService classService=mock(ClassService.class);
        when(classService.getClass(1)).thenReturn(null);
        classResource.setService(classService);
        assertThrows(NotFoundException.class,()->classResource.getClass(1));
        verify(classService,times(1)).getClass(1);
    }
    @Test
    @Tag("get")
    public void getClassWillGetClassWithSameIdAsRequested(){
        when(classService.getClass(1)).thenReturn(new Class(1,"Science"));
        classResource.setService(classService);
        Class testClass=classResource.getClass(1);
        assertEquals(testClass.getId(),1);
    }
    @Test
    @Tag("post")
    public void addClassWillThrowAnExceptionIfItemAlreadyExists(){
        when(classService.getClass(1)).thenReturn(new Class(1,"Science"));
        classResource.setService(classService);
        assertThrows(NotAcceptableException.class,()->classResource.addClass(new Class(1,"cooking")));
    }
    @Test
    @Tag("post")
    public void addClassWillAddClassIfIdIsAvaliable(){
        Class testClass=new Class(1,"electronics");
        when(classService.getClass(1)).thenReturn(null);
        when(classService.addClass(testClass)).thenReturn(testClass);
        classResource.setService(classService);
        assertEquals(classResource.addClass(testClass),testClass);
    }
    @Test
    @Tag("del")
    public void deleteClassWillThrowExceptionIfClassNotFound(){
        Class testClass=new Class(1,"electronics");
        when(classService.getClass(1)).thenReturn(null);
        classResource.setService(classService);
        assertThrows(Exception.class,()->classResource.deleteClass(1));
    }
    @Test
    @Tag("del")
    public void deleteClassWillDeleteClassIfExists(){
        Class testClass=new Class(1,"electronics");
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertNull(classResource.deleteClass(1));
    }
    @Test
    @Tag("get")
    public void getStudentInClassWillThrowExceptionIfClassIsEmpty(){
        when(classService.getClass(1)).thenReturn(null);
        classResource.setService(classService);
        assertThrows(NotFoundException.class,()->classResource.getStudentInClass(1,1));
    }
    @Test
    @Tag("get")
    public void getStudentInClassWillThrowExceptionIfStudentWithIdIsNotEnrolled(){
        Class testClass=new Class(1,"Science");
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertThrows(NotFoundException.class,()->classResource.getStudentInClass(1,1));
    }
    @Test
    @Tag("get")
    public void getStudentInClassWillGetStudentWithIdIfClassExistsAndStudentExists(){
        Class testClass=new Class(1,"Science");
        Student student=new Student(1,"sus",1956,"EECE");
        testClass.setStudentMap(student);
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertEquals(classResource.getStudentInClass(1,1).getId(),1);
    }
    @Test
    @Tag("post")
    public void addStudentInClassWillThrowExceptionIfClassIsEmpty(){
        Student student=new Student(1,"sus",1956,"EECE");
        when(classService.getClass(1)).thenReturn(null);
        classResource.setService(classService);
        assertThrows(NotFoundException.class,()->classResource.addStudentToClass(1,student));
    }
    @Test
    @Tag("post")
    public void addStudentInClassWillAddStudentWithIdIfClassExistsAndStudentDoesnotExist(){
        Class testClass=new Class(1,"Science");
        Student teststudent=new Student(1,"sus",1956,"EECE");
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertNull(classResource.addStudentToClass(1,teststudent));
    }
    @Test
    @Tag("post")
    public void addStudentInClassWillThrowExceptionIfStudentAlreadyExists(){

        Class testClass=new Class(1,"Science");
        Student teststudent=new Student(1,"sus",1956,"EECE");
        testClass.setStudentMap(teststudent);
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertThrows(NotAcceptableException.class,()->classResource.addStudentToClass(1,teststudent));
    }
    @Test
    @Tag("del")
    public void deleteStudentFromClassWillThrowExceptionIfClassIsNotFound(){
        Student student=new Student(1,"sus",1956,"EECE");
        when(classService.getClass(1)).thenReturn(null);
        classResource.setService(classService);
        assertThrows(NotFoundException.class,()->classResource.deleteStudentFromClass(1,1));
    }
    @Test
    @Tag("del")
    public void deleteStudentFromClassWillThrowExceptionIfStudentIsNotFound(){
        Class testClass=new Class(1,"Science");
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertThrows(Exception.class,()->classResource.deleteStudentFromClass(1,1));
    }
    @Test
    @Tag("del")
    public void deleteStudentFromClassWillDeleteStudentIfBothStudentAndClassExists(){
        Class testClass=new Class(1,"Science");
        Student student=new Student(1,"sus",1956,"EECE");
        testClass.setStudentMap(student);
        when(classService.getClass(1)).thenReturn(testClass);
        classResource.setService(classService);
        assertEquals(classResource.deleteStudentFromClass(1,1),student);
    }

}
