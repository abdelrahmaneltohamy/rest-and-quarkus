package org.acme.resteasy.service;

import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.model.Class;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
// spy
public class ClassServiceTest {
    @Spy
    DatabaseClass databaseClass;
    ClassService classService;
    @BeforeEach
    public void setup(){
        databaseClass= spy(new DatabaseClass());

        classService= new ClassService();
    }
    @Test
    public void serviceCanAddAClass(){
        doReturn( new HashMap<>()).when(databaseClass).getClasses();
        classService.addClass(new Class(2,"physics"));
        assertEquals(ClassService.classes.get(2),classService.getClass(2));
    }
    @Test
    public void serviceCanDeleteClass(){
        classService.deleteClass(2);
        assertNull(ClassService.classes.get(2));

    }


}
