package org.acme.resteasy.recourses;

import org.acme.resteasy.model.Class;
import org.acme.resteasy.model.Student;
import org.acme.resteasy.model.Student;
import org.acme.resteasy.service.ErrorService;
import org.acme.resteasy.service.StudentService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/students")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StudentResource {
    StudentService studentService=new StudentService();
    ErrorService errorService= new ErrorService();
    @GET
    public List<Student> getStudents() {
        List<Student> students=studentService.getStudentes();
        if(students.isEmpty())
            throw new NotFoundException(errorService.notFoundResponse());
        return students;
    }
    @GET
    @Path("/{id}")
    public Student getStudent(@PathParam("id") int id) {
        Student student=studentService.getStudent(id);
        if(student==null)
            throw new NotFoundException(errorService.notFoundResponse());
        return student;
    }


    @POST
    public Student addStudent(Student targetStudent){
        if(studentService.getStudent(targetStudent.getId())!=null){
            throw new NotAcceptableException(errorService.itemAlreadyExistsResponse());
        }
        return studentService.addStudent(targetStudent);
    }
    @PUT
    @Path("/{id}")
    public Student updateStudent(@PathParam("id") int id, Student targetStudent){
        targetStudent.setId(id);
        if(studentService.getStudent(targetStudent.getId())==null){
            throw new NotFoundException(errorService.notFoundToUpdateResponse());
        }
        return studentService.updateStudent(targetStudent);
    }
    @DELETE
    @Path("/{id}")
    public Student deleteStudent(@PathParam("id") int id) {
        if(studentService.getStudent(id)==null){
            throw new NotFoundException(errorService.notFoundToDeleteResponse());
        }
        return studentService.deleteStudent(id);
    }
/*
    @GET
    @Path("/{id}/classes")
    public List<Class> getClassesEnrolledForStudent(@PathParam("id") int id){
        Student student=studentService.getStudent(id);
        return studentService.getClasses(student);
    }

  @POST
    @Path("/{id}/classes")
    public Class addClassToStudent(@PathParam("id") int id,Class targetClass){

        Student student=studentService.getStudent(id);
        return studentService.setClassToStudent(student,targetClass);
    }
   @GET
    @Path("/{id}/classes/{classId}")
    public Class getClassFromStudent(@PathParam("id") int id,@PathParam("classId") int classId) {
        return studentService.getStudent(id).getClassMap(classId);
    }
    @PUT
    @Path("/{id}/classes/{classId}")
    public Class updateClassFromStudent(@PathParam("id") int id, Class targetClass,@PathParam("classId") int classId){
        targetClass.setId(classId);
        return studentService.updateClassFromStudent(targetClass,studentService.getStudent(id));
    }
    @DELETE
    @Path("/{id}/classes/{classId}")
    public Class deleteClassFromStudent(@PathParam("id") int id,@PathParam("classId") int classId) {
        return studentService.deleteClassFromStudent(id,classId);
    }
*/

}
