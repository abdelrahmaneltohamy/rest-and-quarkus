package org.acme.resteasy.recourses;

import org.acme.resteasy.model.Message;
import org.acme.resteasy.service.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/resteasy/messages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MessageResource {
    MessageService messageService = new MessageService();
    @GET
    public List<Message> getMessages() {
        return messageService.getMessages();
    }
    @GET
    @Path("/{id}")
    public Message getMessage(@PathParam("id") int id) {
        return messageService.getMessage(id);
    }
    @POST
    public Message addMessage(Message message){
        return messageService.addMessage(message);
    }
    @PUT
    @Path("/{id}")
    public Message updateMessage(@PathParam("id") int id, Message message){
        message.setId(id);
        return messageService.updateMessage(message);
    }
    @DELETE
    @Path("/{id}")
    public Message deleteMessage(@PathParam("id") int id) {
        return messageService.deleteMessage(id);
    }

}
