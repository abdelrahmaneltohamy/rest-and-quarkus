package org.acme.resteasy.recourses;

import org.acme.resteasy.model.Class;

import org.acme.resteasy.model.Student;
import org.acme.resteasy.service.ClassService;
import org.acme.resteasy.service.ErrorService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;

@Path("/classes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClassResource {

    ClassService classService = new ClassService();
    ErrorService errorService= new ErrorService();

    @GET
    public List<Class> getClasses() {
        List<Class> classes =classService.getClasses();
        if(classes.isEmpty()){

            throw new NotFoundException(errorService.notFoundResponse());
        }
        return classes;
    }

    @GET
    @Path("/{id}")
    public Class getClass(@PathParam("id") int id) {
        Class myClass = classService.getClass(id);
        if(myClass==null){
            throw new NotFoundException(errorService.notFoundResponse());
        }
        return myClass;

    }

    @POST
    public Class addClass(Class targetClass){
        if(classService.getClass(targetClass.getId())!=null){
            throw new NotAcceptableException(errorService.itemAlreadyExistsResponse());
        }
        return classService.addClass(targetClass);
    }

    @PUT
    @Path("/{id}")
    public Class updateClass(@PathParam("id") int id, Class targetClass){
        targetClass.setId(id);
        if(classService.getClass(targetClass.getId())==null){
            throw new NotFoundException(errorService.notFoundToUpdateResponse());
        }
        return classService.updateClass(targetClass);
    }

    @DELETE
    @Path("/{id}")
    public Class deleteClass(@PathParam("id") int id) {
        if(classService.getClass(id)==null){
            throw new NotFoundException(errorService.notFoundToDeleteResponse());
        }
        return classService.deleteClass(id);
    }

    @GET
    @Path("/{id}/students")
    public List<Student> getClassesEnrolledForStudent(@PathParam("id") int id){
        if(classService.getClass(id)==null){
            throw new NotFoundException(errorService.notFoundToDeleteResponse());
        }
        Class targetClass=classService.getClass(id);
        List<Student> studentsInClass=classService.getStudents(targetClass);
        if(studentsInClass.isEmpty())
            throw new NotFoundException(errorService.noStudentsInClass());
        return studentsInClass;
    }

    @POST
    @Path("/{id}/students")
    public Student addStudentToClass(@PathParam("id") int id,Student student){
        Class targetClass=classService.getClass(id);
        if(targetClass==null){
            throw new NotFoundException(errorService.notFoundResponse());
        }
        if(targetClass.getStudentMap(student.getId())!=null){
            throw new NotAcceptableException(errorService.itemAlreadyExistsResponse());
        }
        return classService.setStudentToClass(targetClass,student);
    }

    @GET
    @Path("/{id}/students/{studentId}")
    public Student getStudentInClass(@PathParam("id") int id,@PathParam("studentId") int studentId) {
        Class targetClass=classService.getClass(id);
        if(targetClass==null)
            throw new NotFoundException(errorService.notFoundResponse());
        Student student=targetClass.getStudentMap(studentId);
        if(student==null)
            throw new NotFoundException(errorService.notFoundResponse());
        return classService.getClass(id).getStudentMap(studentId);
    }

    @PUT
    @Path("/{id}/students/{studentId}")
    public Student updateStudentInClass(@PathParam("id") int id, Student student,@PathParam("studentId") int studentId){
        student.setId(studentId);
        Class targetClass=classService.getClass(id);
        if(targetClass==null)
            throw new NotFoundException(errorService.notFoundResponse());
        if(targetClass.getStudentMap(studentId)==null){
            throw new NotFoundException(errorService.notFoundToUpdateResponse());
        }
        List<Class> classes=classService.getClasses();
        classes.stream()
                .filter(curClass -> curClass.getStudentMap(studentId)!=null)
                .forEach(curClass ->classService.updateStudentinClass(curClass,student));
        return classService.updateStudentinClass(targetClass,student);
    }

    @DELETE
    @Path("/{id}/students/{studentId}")
    public Student deleteStudentFromClass(@PathParam("id") int id,@PathParam("studentId") int studentId) {
        Class targetClass=classService.getClass(id);
        if(targetClass==null)
            throw new NotFoundException(errorService.notFoundResponse());
        Student student=targetClass.getStudentMap(studentId);
        if(student==null)
            throw new NotFoundException(errorService.notFoundToDeleteResponse());
        List<Class> classes=classService.getClasses();
        classes.stream()
                .filter(curClass -> curClass.getStudentMap(studentId)!=null)
                .forEach(curClass ->classService.deleteStudentFromClass(curClass.getId(),studentId));
        return student;
    }
    //for test only
    public void setService(ClassService classService){
        this.classService=classService;
    }
}
