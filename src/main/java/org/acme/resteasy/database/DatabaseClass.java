package org.acme.resteasy.database;

import org.acme.resteasy.model.Class;
import org.acme.resteasy.model.Message;
import org.acme.resteasy.model.Student;

import java.util.HashMap;
import java.util.Map;

public class DatabaseClass {
    private static Map<Integer,Message> messages=new HashMap<>();
    private static Map<Integer,Class> classes=new HashMap<>();
    private static Map<Integer,Student> students=new HashMap<>();


    public static Map<Integer, Message> getMessages() {
        return messages;
    }
    public static Map<Integer, Class> getClasses() {
        return classes;
    }
    public static Map<Integer, Student> getStudents() {
        return students;
    }

}
