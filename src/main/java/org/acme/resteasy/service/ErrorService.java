package org.acme.resteasy.service;

import org.acme.resteasy.model.ErrorMessage;

import javax.ws.rs.core.Response;

public class ErrorService {
    public ErrorService() {
    }

    public Response notFoundResponse(){
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage("not found",404,"no such id is avalible"))
                .build();
    }
    public Response noStudentsInClass(){
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage("not found",404,"no students are enrolled in this class"))
                .build();
    }
    public Response notFoundToUpdateResponse(){
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorMessage("bad request",400,"can't update an item that doesn't exist"))
                .build();
    }
    public Response notFoundToDeleteResponse(){
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorMessage("bad request",400,"can't delete an item that doesn't exist"))
                .build();
    }
    public Response itemAlreadyExistsResponse(){
        return Response.status(Response.Status.NOT_ACCEPTABLE)
                .entity(new ErrorMessage("not acceptable",406,"id already exists return after deleting this id"))
                .build();
    }
}
