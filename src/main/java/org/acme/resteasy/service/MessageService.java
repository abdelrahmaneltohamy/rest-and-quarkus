package org.acme.resteasy.service;

import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.model.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageService {

    static Map<Integer,Message> messages=DatabaseClass.getMessages();
    public MessageService(){

        messages.put(1,new Message(1,"hi","maun"));
        messages.put(2,new Message(2,"hello","maun"));
    }
    public List<Message> getMessages() {
    return new ArrayList<Message>(messages.values());
    }
    public Message getMessage(int id){
        return messages.get(id);
    }
    public Message addMessage(Message message){
        message.setId(messages.size()+1);
        messages.put(message.getId(),message);
        return message;
    }
    public Message updateMessage(Message message){
        if(message.getId()<1){
            return null;
        }
        messages.put(message.getId(),message);
        return message;
    }
    public Message deleteMessage(int id){
        return messages.remove(id);
    }

}
