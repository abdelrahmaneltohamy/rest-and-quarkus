package org.acme.resteasy.service;

import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.model.Class;
import org.acme.resteasy.model.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassService {
    static Map<Integer,Class> classes=DatabaseClass.getClasses();
    public ClassService(){

        classes.put(1,new Class(1,"Math"));
        //classes.put(2,new Class(2,"hello","maun"));
    }
    public List<Class> getClasses() {
        return new ArrayList<Class>(classes.values());
    }

    public List<Student> getStudents(Class classNeeded){
        Class tempClass = classes.get(classNeeded.getId());
        return new ArrayList<Student>(tempClass.getStudentsMap().values());
    }

    public Student setStudentToClass(Class targetClass,Student student){
        StudentService.students.put(student.getId(),student);
        classes.get(targetClass.getId()).setStudentMap(student);
        return student;
    }
    public Class getClass(int id){
        return classes.get(id);
    }
    public Class addClass(Class message){
        message.setId(classes.size()+1);
        classes.put(message.getId(),message);
        return message;
    }
    public Class updateClass(Class message){
        if(message.getId()<1){
            return null;
        }
        classes.put(message.getId(),message);
        return message;
    }
    public Student updateStudentinClass(Class targetClass, Student student){
        if(student.getId()<1){
            return null;
        }
        //   updatedClass.setStudentMap(student);
        StudentService.students.put(student.getId(),student);
        classes.get(targetClass.getId()).setStudentMap(student);
        //students.put(message.getId(),message);
        return student;
    }
    public Class deleteClass(int id){
        return classes.remove(id);
    }
    public Student deleteStudentFromClass(int id, int studentId){
        if(StudentService.students.get(studentId)!=null)
            StudentService.students.remove(studentId);
        return classes.get(id).getStudentsMap().remove(studentId);
    }


    
}
