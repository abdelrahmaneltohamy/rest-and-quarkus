package org.acme.resteasy.service;

import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.model.Class;
import org.acme.resteasy.model.Student;
import org.acme.resteasy.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentService {
    static Map<Integer,Student> students=DatabaseClass.getStudents();
    public StudentService(){

        students.put(1,new Student(1,"maun",2018,"EECE"));
        //students.put(2,new Student(2,"hello","maun"));
    }
    public List<Student> getStudentes() {
        return new ArrayList<Student>(students.values());
    }

    public List<Class> getClasses(Student student){
        Student tempStudent = students.get(student.getId());
        return new ArrayList<Class>(tempStudent.getClassesMap().values());
    }


    public Class setClassToStudent(Student targetStudent,Class targetClass){
        //targetClass.setStudentMap(targetStudent);
        ClassService.classes.put(targetClass.getId(),targetClass);
        students.get(targetStudent.getId()).setClassMap(targetClass);
        return targetClass;
    }
    public Student getStudent(int id){
        return students.get(id);
    }
    public Student addStudent(Student message){
        message.setId(students.size()+1);
        students.put(message.getId(),message);
        return message;
    }
    public Student updateStudent(Student message){
        if(message.getId()<1){
            return null;
        }
        students.put(message.getId(),message);
        return message;
    }
    public Class updateClassFromStudent(Class updatedClass,Student student){
        if(student.getId()<1){
            return null;
        }
     //   updatedClass.setStudentMap(student);
        ClassService.classes.put(updatedClass.getId(),updatedClass);
        students.get(student.getId()).setClassMap(updatedClass);
        //students.put(message.getId(),message);
        return updatedClass;
    }

    public Student deleteStudent(int id){
        return students.remove(id);
    }
    public Class deleteClassFromStudent(int id, int classId){
        ClassService.classes.remove(classId);
        return students.get(id).getClassesMap().remove(classId);
    }


}
