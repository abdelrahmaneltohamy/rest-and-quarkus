package org.acme.resteasy.model;

public class ClasstoStudentIdentifier {
    int id;
    int studentId;
    int classId;

    public ClasstoStudentIdentifier() {
    }

    public ClasstoStudentIdentifier(int id, int studentId, int classId) {
        this.id = id;
        this.studentId = studentId;
        this.classId = classId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }
}
