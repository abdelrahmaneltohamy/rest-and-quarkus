package org.acme.resteasy.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Class {
    int id;
    String name;
    Map<Integer,Student> studentsMap = new HashMap<>();
    List<Integer> studentsid = new ArrayList<>();


    public Class() {
    }

    public Class(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Integer, Student> getStudentsMap() {
        return studentsMap;
    }

    public void setStudentsMap(Map<Integer, Student> studentsMap) {
        this.studentsMap = studentsMap;
    }
    public void setStudentMap(Student studentMap){
        this.studentsMap.put(studentMap.getId(),studentMap);
    }
    public Student getStudentMap(int id){
        return this.studentsMap.get(id);
    }

    public List<Integer> getStudentsid() {
        return studentsid;
    }

    public void setStudentsid(List<Integer> studentsid) {
        this.studentsid = studentsid;
    }
}
