package org.acme.resteasy.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@XmlRootElement
public class Student {
    int id;
    String name;
    int EnrolmentYear;
    String major;
    List<Integer> studentsid = new ArrayList<>();
    Map<Integer, Class> classesMap = new HashMap<>();

    public Student() {

    }

    public Student(int id, String name, int enrolmentYear, String major) {
        this.id = id;
        this.name = name;
        this.EnrolmentYear = enrolmentYear;
        this.major = major;


    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnrolmentYear() {
        return EnrolmentYear;
    }

    public void setEnrolmentYear(int enrolmentYear) {
        EnrolmentYear = enrolmentYear;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Map<Integer, Class> getClassesMap() {
        return classesMap;
    }

    public void setClassesMap(Map<Integer, Class> classesMap) {
        this.classesMap = classesMap;
    }
    public Class getClassMap(int id){
        return classesMap.get(id);
    }
    public void setClassMap(Class classAdded) {
        this.classesMap.put(classAdded.getId(),classAdded);
    }
}
