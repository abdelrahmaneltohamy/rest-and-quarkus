package org.acme.resteasy.model;

public class ErrorMessage {
    String statueText;
    int statueCode;
    String additionalRefrences;

    public ErrorMessage(String statueText, int statueCode, String additionalRefrences) {
        this.statueText = statueText;
        this.statueCode = statueCode;
        this.additionalRefrences = additionalRefrences;
    }

    public ErrorMessage() {

    }

    public String getStatueText() {
        return statueText;
    }

    public void setStatueText(String statueText) {
        this.statueText = statueText;
    }

    public int getStatueCode() {
        return statueCode;
    }

    public void setStatueCode(int statueCode) {
        this.statueCode = statueCode;
    }

    public String getAdditionalRefrences() {
        return additionalRefrences;
    }

    public void setAdditionalRefrences(String additionalRefrences) {
        this.additionalRefrences = additionalRefrences;
    }
}
